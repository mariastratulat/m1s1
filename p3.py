from collections import OrderedDict

def name_bdyear(name, bdyear):

    my_dict =  dict(zip(name, bdyear))
    # print(my_dict)
    value = sorted(my_dict.values())
    # print(value)
    new_dict = {}
    for v in value:
        for k, j in my_dict.items():
            if v == j:
                new_dict[k] = v
    return new_dict


print(name_bdyear(['Gabi', 'Andreea', 'Paul'], [1993, 1989, 1997]))

od = OrderedDict()
def name_bdyear1(name, bdyear):

    my_dict =  dict(zip(name, bdyear))
    # print(my_dict)
    return dict(OrderedDict(sorted(my_dict.items(), key = lambda t: t[1])))


print(name_bdyear1(['Gabi', 'Andreea', 'Paul'], [1993, 1989, 1997]))
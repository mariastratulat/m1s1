# Nu stiu cum poate functiona aceasta functie si pentru str
# Varianta mea este valabilal pentru list si tuple

def squared(x):
    return x**2

def funct_5(f, items):
    result = [f(item) for item in items]
    t = type(items)
    return t(result)

print(funct_5(squared, (1, 1, 2, 3, 5, 8)))


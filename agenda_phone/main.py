import sys


def show_menu(items):
    for item in items:
        print(item + ' - ' + items[item])


def exit(items):
    return sys.exit()


def show_agenda(items):
    if items == {}:
        print('You have no contacts')
    for item in items:
        print(item)


def add_contact(items):
    name = input('Name: ')
    phone_number = input('Phone number: ')
    items[name].append(phone_number)
    print('%s is added' % name)


def add_phone_nr(items):
    result = find_contact(items)
    print('Adding a new phone number to %s' % result)
    phone_number = input('Phone number: ')
    if phone_number != '':
        items[result].append(phone_number)
        print('%s with phone number %s is added' % (result, phone_number))
    else:
        print('Nothing to add')


def find_contact(items):
    target = input('Type name: ')
    result = [key for key in items.keys() if target in key]
    if not result:
        print('No contact with this name')
        return
    for num, elem in enumerate(result):
        print(num, elem)
    chosen = input('Which one? Type the number: ')
    try:
        print(result[int(chosen)])
        return result[int(chosen)]
    except IndexError:
        print('Oops! No such number.')


def delete_contact(items):
    name = find_contact(items)
    if name == None:
        return
    message = input('Do you want to delete %s from contacts? (Y/N)' % name)
    if message == 'Y' or message == 'y':
        del items[name]
        print('%s has been deleted' % name)

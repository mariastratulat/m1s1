from collections import defaultdict

from main import exit, show_agenda, add_contact, add_phone_nr

from main import find_contact, delete_contact


menu = {
    '1': 'Show agenda',
    '2': 'Add contact',
    '3': 'Add phone nr. to contact',
    '4': 'Find contact',
    '5': 'Delete contact',
    '0': 'Exit'
}


run = {
    '0': exit,
    '1': show_agenda,
    '2': add_contact,
    '3': add_phone_nr,
    '4': find_contact,
    '5': delete_contact
}


agenda = defaultdict(list)
agenda['ion'].append('0747613540')
agenda['maria'].append('0747613541')
agenda['mara'].append('0747613542')

from main import show_menu

from data import menu, agenda, run


print('*' * 15 + ' MENU ' + '*' * 15)

show_menu(menu)

print('*' * 37)

your_input = input('Your option: ')


def running(option, my_dict):
    for item in my_dict:
        if option == item:
            return my_dict[item](agenda)


while your_input != "":
    running(your_input, run)
    your_input = input("Your option: ")

def min_max1(*args):
    return min(*args), max(*args)


def min_max2(*args):
    num_max = args[0]
    num_min = args[0]
    for n in args:
        if n > num_max:
            num_max = n
        if n < num_min:
            num_min = n
    return num_min, num_max


print(min_max2(73, 21, 7, 321, 5, 8))
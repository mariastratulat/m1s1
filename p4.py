def traveler(countries, visited, to_visit):
    c = open(countries).readlines()
    v = open(visited).readlines()
    t = open(to_visit).readlines()
    target = open(to_visit, 'w')
    to_go = [x for x in c if x not in v]
    #print(to_go)
    for item in to_go:
        if item not in t:
            target.write(item)
    return "The countries to visit are in %s file" % to_visit
    target.close()

print(traveler('countries.txt', 'visited.txt', 'to_be_visited.txt'))